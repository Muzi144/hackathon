import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.file.StandardOpenOption;

import java.security.MessageDigest;
import java.util.Scanner;


public class Security {

	static void security_service(String input,FileWriter writer) throws IOException{
		// Authentication format i.e input : username@password

	    String username;
	    String password;
	    String line;
	    String f_user;
	    String f_pass;
	    int user_flag = 0;

	    username = input.split("@")[0];
	    password = input.split("@")[1];

	    InputStream fis = new FileInputStream("login_credentials.txt");
        InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
        BufferedReader br = new BufferedReader(isr);

        while ((line = br.readLine()) != null) {
        	f_user = line.split("@")[0];
        	f_pass = line.split("@")[1];

        	if(username.equals(f_user) && password.equals(f_pass)){
        		user_flag = 1;
    	        break;
        	}
        }
        if(user_flag == 1){
        	System.out.println("You are logged in");
        }
        else{
        	System.out.println("login credentials or login format is wrong");
        	System.out.println("login format :  username@password");
        }


       if(user_flag == 1){
		   // create token
    	   String token  = token_creation(input);
		   System.out.println("token: " + token);

		   //write a token file to keep records
		   writer.write((input + "@"  + token + "@\n"));
		   writer.close();
       }
}

	static String token_creation(String input){
		//String plaintext = "your text here";
		MessageDigest msg;
		String token = null;
		try {
			msg = MessageDigest.getInstance("MD5");
			msg.reset();
			msg.update(input.getBytes());
			byte[] digest = msg.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			String hashtext = bigInt.toString(16);
			// Now we need to pad zeroes it if you actually want the full 32 chars.
			while(hashtext.length() < 32 ){
			  hashtext = "0"+hashtext;
			}
			token = hashtext;
			//System.out.println("hash: " + hashtext);
		}

		catch (java.security.NoSuchAlgorithmException e){
		}
		return token;
	}

	public static void main(String[] Args) throws FileNotFoundException, IOException{
		String line;
		int token_flag = 0;
		Scanner in = new Scanner(System.in);
		String auth = in.next();
		//create a token file
		    String filename= "token.txt";
		    FileWriter fw = new FileWriter(filename,true);
		    
		//if the request is login i.e creating token
		    security_service(auth,fw);

		// else get tokens from token file one by one and check if input_token is present in the file or not
	        InputStream fis = new FileInputStream(filename);
	        InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
	        BufferedReader br = new BufferedReader(isr);

	        while ((line = br.readLine()) != null) {

	        	String token = line.split("&")[2];
	        	System.out.println("t:" + token);
	        	if (token.equals(input_token)){
	        		token_flag = 1;
	        		break;
	        	}
	        }
        if (token_flag == 0)
        	System.out.println("You first login or token is wrong");
        else
        	System.out.println("Token Authorization Successfull");

	}

}

